Projeto PSD 2021/2022
Alunos: João Mateus Colet e Carolina Simões

O arquivo PDF contém o reatório e analise de resultados obtidos.

Na pasta ALUX temos os arquivo RTL da alux e os modulos dependentes da alux, além do Testbench
Na pasta RegisterBank o arquivo rtl do sistema e seu testbench

Para o ALUX funcionar é necessário dentro do arquivo ATAN_ROM.v definir o caminho do arquivo atanLUDd.hex no parametro ATANLUT_FILENAME